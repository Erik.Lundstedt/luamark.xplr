local xplr = xplr
local function setup(args)
  if (args == nil) then
    args = {}
  else
  end
  if (args.mode == nil) then
    args.mode = "default"
  else
  end
  if (args.key1 == nil) then
    args.key1 = "ctrl-b"
  else
  end
  if (args.directory == nil) then
    args.directory = (os.getenv("XDG_CONFIG_DIR") .. "/xplr-quickmarks")
  else
  end
  local function cd(target)
    return {ChangeDirectory = target}
  end
  xplr.config.modes.builtin[args.mode].key_bindings.on_key[args.key1] = {help = "luamark", messages = {cd(args.directory)}}
  return nil
end
return {setup = setup}
