(local xplr xplr)
(fn setup [args]

  (when (= args nil)
    (set-forcibly! args {}))
  (when (= args.mode nil)
    (set args.mode :default))
  (when (= args.key1 nil)
    (set args.key1 :ctrl-b))
  (when (= args.directory nil)
    (set args.directory (.. (os.getenv "XDG_CONFIG_DIR")  "/xplr-quickmarks"))
    )

  (fn cd [target]
    {:ChangeDirectory target }
    )



  (tset (. (. (. xplr.config.modes.builtin args.mode) :key_bindings) :on_key) args.key1
        {
         :help "luamark"
         :messages
         [(cd args.directory)]
         }
        )
  )

{:setup setup}
