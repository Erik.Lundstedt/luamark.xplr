--*- mode: kakapo; mode: whitespace; mode: lua; tab-width: 4; indent-tabs-mode: t; lua-indent-level: 4; -*-
local api = require("plugins.luamark.lib.luamark")
local xplr = xplr
Globals={
	["selector"]=1,
	["marksCount"]=0,
	["marks"]={
	}
}

local function dump(o)
	if type(o) == "table" then
		local s = "{ "
		for k, v in pairs(o) do
			if type(k) ~= "number" then
				k = '"' .. k .. '"'
			end
			s = s .. "[" .. k .. "] = " .. dump(v) .. ",\n"
		end
		return s .. "}\n "
	else
		return tostring(o)
	end
end

local function switch(case, cases, defCase, ...)
	if cases[case] then
		if type(cases[case]) == "function" then
			return cases[case](...)
		elseif type(cases[case]) == "number"
			or type(cases[case]) == "string" then
			return cases[cases[case] ](...)
		else error("Case element must be a number, string, or function.", 2) end
	else return defCase(...) end
end



local function setup(args)
	local xplr = xplr
	if args == nil then
		args = {}
	end
	if args.mode == nil then
		args.mode = "default"
	end
	if args.key1 == nil then
		args.key1 = "ctrl-b"
	end
	if args.key2 == nil then
		args.key2 = "ctrl-x"
	end

	local layoutWidths={
	{ Percentage = 12 },--mark
	{ Percentage = 10 },--index
	{ Percentage = 15 },--name
	{ Percentage = 8  },--spacer
	{ Percentage = 40 },--uri
	}



	local luamarkList = {
		CustomContent = {
			title = "luamarks",
			body = {
				DynamicTable = {
					widths = layoutWidths,
					col_spacing = 1,
					render = "custom.render_layout",
				},
			},
		},}

	local header = {
		CustomContent = {
			title = "luamarks",
			body = {
				StaticTable = {
					widths = layoutWidths,
					col_spacing = 1,
					render = {
						{"[selected]","[index]","[name]","","[path]"}
					},
				},
			},
		},}

	xplr.fn.custom.render_layout = function(ctx)
		local items={}
		local i=1

		local function isMarked(j)
			if Globals["selector"]==j then
				return "[#]======>"
			else
				return "|||       "
			end
		end





		for k, v in pairs(api.getMarks()) do
		items[i]={isMarked(i),"[ "..i.." ] ", v["name"],"=====>", v["uri"]}
			Globals["marks"][i]={name=v.name,uri=v.uri,key=k}
			i=i+1
		end
		Globals["marksCount"]=i
		Globals["items"]=items
		return items
		--return {{items[1]["name"], items[1]["uri"] }}
	end



	local luamarkLayout={
		Horizontal = {
			config = {
				margin = nil,
				horizontal_margin = 0,
				vertical_margin = 0,
				constraints = {
					{ Percentage = 70 },
					{ Percentage = 30 },
				},
			},
			splits = {
				{
					Vertical = {
						config = {
							margin = 0,
							horizontal_margin = nil,
							vertical_margin = nil,
							constraints = {
								{ Length = 3 },
								{ Min = 1 },
								{ Length = 3 },
							},
						},
						splits = {
							header,
							luamarkList,
							"InputAndLogs",
						},
					},
				},
				{
					Vertical = {
						config = {
							margin = 0,
							horizontal_margin = nil,
							vertical_margin = nil,
							constraints = {
								{ Percentage = 50 },
								{ Percentage = 50 },
							},
						},
						splits = {
							"Selection",
							"HelpMenu",
						},
					},
				},
			},
		},}



xplr.fn.custom.setSelector=function(action,arg)
	local sel=Globals["selector"]
	local max=Globals["marksCount"]
	switch(action,{
			["add"]=function()
				if sel + arg < max then
					Globals["selector"]=Globals["selector"]+arg
				end
			end,
			["subtract"]=function()
			if sel-arg > 0 then
					Globals["selector"]=Globals["selector"]-arg
				end
			end,
				},
			function()
	end)end

xplr.fn.custom.setSelector_add=function()
		xplr.fn.custom.setSelector([[add]],1)
	end

xplr.fn.custom.setSelector_subtract=function()
		xplr.fn.custom.setSelector([[subtract]],1)
	end


xplr.fn.custom.openMark = function(_)
	return {
		{LogInfo = dump(Globals)},
		{ChangeDirectory = Globals["marks"][Globals["selector"]]["uri"]},
		"PopMode"
	}
	end


local goBack={
	help = "back",
	messages = {"PopMode",
				{ SwitchModeBuiltin = "default" }
}}

local openMark={
	help = "open mark",
	messages = {
		{ CallLua = "custom.openMark"},
	}
}



xplr.config.modes.custom.luamark = {
	name = "luamark",
	layout = luamarkLayout,
	key_bindings = {
		on_key = {
			["d"] = {
				help = "global help menu",
				messages = {
					{
						BashExec = [===[
				[ -z "$PAGER" ] && PAGER="less -+F"
				cat -- "${XPLR_PIPE_LOGS_OUT}" | ${PAGER:?}
									]===],
					},
				},
			},
			enter  = openMark,
			right  = openMark,
			["l"]  = openMark,
			left   = goBack,
			["q"]  = goBack,
			["h"]  = goBack,
		["ctrl-c"] = goBack,
			down = {
				help = "down",
				messages = {{CallLuaSilently="custom.setSelector_add"}}
			},
			up = {
				help = "up",
				messages = {{CallLuaSilently="custom.setSelector_subtract"}}
			},
			["j"] = {
				help = "down",
				messages = {{CallLuaSilently="custom.setSelector_add"}}
			},
			["k"] = {
				help = "up",
				messages = {{CallLuaSilently="custom.setSelector_subtract"}}
				}
		}
	}
}

xplr.config.modes.builtin[args.mode].key_bindings.on_key[args.key1] = {
		help = "luamark-mode",
		messages = {
			{
				ChangeDirectory="/home/erik/.config/bookmarks"
				--				SwitchModeCustom = "luamark"
			}
				--LuaEval
				--LogInfo = dump(api.getAll())
				--LogInfo = dump(xplr.fn.custom.render_layout)
			--"PopMode",
		},
	}

local function getLast(s, delimiter)
	local result = ""
	for match in (s..delimiter):gmatch("(.-)"..delimiter) do
		result=match
	end
	return result;
end


xplr.fn.custom.addMark=function(ctx)
	api.put(getLast(ctx.pwd,"/"), ctx.pwd )
	return {
	{LogInfo="added new bookmark named : "..getLast(ctx.pwd,"/").." to "..ctx.pwd}
	}
end


xplr.config.modes.builtin[args.mode].key_bindings.on_key[args.key2] = {
		help = "luamark put pwd[\"name\"] pwd",
		messages = {
			{
				CallLua="custom.addMark"
			}
				--LuaEval
				--LogInfo = dump(api.getAll())
				--LogInfo = dump(xplr.fn.custom.render_layout)
			--"PopMode",
		},
}







end




return { setup = setup }
